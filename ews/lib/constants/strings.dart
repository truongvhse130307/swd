class Strings {
  static String user_url = 'https://myews.azurewebsites.net/api/v1/Users';
  static String store_url = 'https://myews.azurewebsites.net/api/v1/Stores';
  static String brand_url = 'https://myews.azurewebsites.net/api/v1/Brands';
  static String category_url =
      'https://myews.azurewebsites.net/api/v1/Categories';
  static String counter_url = 'https://myews.azurewebsites.net/api/v1/Counters';
  static String device_url = 'https://myews.azurewebsites.net/api/v1/Devices';
  static String deviceType_url =
      'https://myews.azurewebsites.net/api/v1/DeviceTypes';
  static String invoice_url = 'https://myews.azurewebsites.net/api/v1/Invoices';
  static String record_url = 'https://myews.azurewebsites.net/api/v1/Records';
  static String role_url = 'https://myews.azurewebsites.net/api/v1/Roles';
}
