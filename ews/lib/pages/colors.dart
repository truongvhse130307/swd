import 'package:flutter/material.dart';

const Color myPrimaryColor = Color(0xFF1E88E5);
const Color myWarningColor = Color(0xFFda4167);
const Color mywhiteColor = Color(0xFFFFFFFF);
const Color myblackColor = Color(0xFF000000);