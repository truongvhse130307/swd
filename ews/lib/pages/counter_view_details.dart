import 'package:ews/models/recordInfo.dart';
import 'package:ews/resource/api_manager.dart';
import 'package:flutter/material.dart';
import 'colors.dart';
import 'history_counter.dart';
import 'history_invoice.dart';
import 'home.dart';
import 'more.dart';

class CounterViewDetails extends StatefulWidget {
  @override
  _CounterViewDetails createState() => _CounterViewDetails();
  final int id;
  final int category;
  CounterViewDetails({Key key, @required this.id, @required this.category})
      : super(key: key);
}

class _CounterViewDetails extends State<CounterViewDetails> {
  Future<Record> _record;
  String _value = "Điện";
  @override
  void initState() {
    _record = API_Manager().getRecordByID(widget.id);
    super.initState();
    if (widget.category == 1) {
      _value = "Điện";
    } else {
      _value = "Nước";
    }
  }

  @override
  Widget build(BuildContext context) {
    int bottomBarIndex = 1;
    return MaterialApp(
      home: Scaffold(
        appBar: (AppBar(
            toolbarHeight: 60,
            centerTitle: true,
            title: Text("Chi tiết hoá đơn",
                style: TextStyle(
                  fontSize: 27,
                  color: Colors.white,
                )))),
        body: SafeArea(
          child: ListView(
            padding: EdgeInsets.symmetric(vertical: 15.0),
            children: <Widget>[
              // Details(),
              Padding(
                padding: const EdgeInsets.all(20.0),
                child: SingleChildScrollView(
                  child: Column(
                    children: <Widget>[
                      FutureBuilder<Record>(
                          future: _record,
                          builder: (context, snapshot) {
                            if (snapshot.hasData) {
                              return ListView.builder(
                                  shrinkWrap: true,
                                  itemCount: 1,
                                  itemBuilder: (context, index) {
                                    var record = snapshot.data;
                                    return Column(children: <Widget>[
                                      Row(
                                        mainAxisAlignment:
                                            MainAxisAlignment.spaceBetween,
                                        children: <Widget>[
                                          Column(
                                            children: <Widget>[
                                              Text(
                                                "Mã hoá đơn:",
                                                style: TextStyle(
                                                    fontSize: 18,
                                                    fontWeight:
                                                        FontWeight.bold),
                                              ),
                                            ],
                                          ),
                                          Column(
                                            children: <Widget>[
                                              Text("${record.id}",
                                                  style:
                                                      TextStyle(fontSize: 18)),
                                            ],
                                          )
                                        ],
                                      ),
                                      SizedBox(
                                        height: 20,
                                      ),
                                      Row(
                                        mainAxisAlignment:
                                            MainAxisAlignment.spaceBetween,
                                        children: <Widget>[
                                          Text(
                                            "Loại hoá đơn:",
                                            style: TextStyle(
                                                fontSize: 18,
                                                fontWeight: FontWeight.bold),
                                          ),
                                          Text(_value,
                                              style: TextStyle(fontSize: 18))
                                        ],
                                      ),
                                      SizedBox(
                                        height: 20,
                                      ),
                                      Row(
                                        mainAxisAlignment:
                                            MainAxisAlignment.spaceBetween,
                                        children: <Widget>[
                                          Text(
                                            "Số mới:",
                                            style: TextStyle(
                                                fontSize: 18,
                                                fontWeight: FontWeight.bold),
                                          ),
                                          Text("${record.countNumber}",
                                              style: TextStyle(fontSize: 18))
                                        ],
                                      ),
                                      SizedBox(
                                        height: 20,
                                      ),
                                      Row(
                                        mainAxisAlignment:
                                            MainAxisAlignment.spaceBetween,
                                        children: <Widget>[
                                          Text(
                                            "Tên cửa hàng:",
                                            style: TextStyle(
                                                fontSize: 18,
                                                fontWeight: FontWeight.bold),
                                          ),
                                          Text("The Coffee House",
                                              style: TextStyle(fontSize: 18))
                                        ],
                                      ),
                                      SizedBox(
                                        height: 20,
                                      ),
                                      Row(
                                        mainAxisAlignment:
                                            MainAxisAlignment.spaceBetween,
                                        children: <Widget>[
                                          Text(
                                            "Ngày tạo:",
                                            style: TextStyle(
                                                fontSize: 18,
                                                fontWeight: FontWeight.bold),
                                          ),
                                          Text("${record.createdTime}",
                                              style: TextStyle(fontSize: 18))
                                        ],
                                      ),
                                      SizedBox(
                                        height: 20,
                                      ),
                                    ]);
                                  });
                            } else {
                              return Center(child: CircularProgressIndicator());
                            }
                          })
                    ],
                  ),
                ),
              )
            ],
          ),
        ),
        // bottom bar
        bottomNavigationBar: BottomNavigationBar(
          backgroundColor: Colors.white,
          selectedItemColor: myPrimaryColor,
          unselectedItemColor: Colors.grey,
          currentIndex: bottomBarIndex,
          items: [
            BottomNavigationBarItem(
              icon: Icon(Icons.home_outlined),
              label: "Trang chủ",
            ),
            BottomNavigationBarItem(
              icon: Icon(Icons.history_outlined),
              label: "Công tơ",
            ),
            BottomNavigationBarItem(
              icon: Icon(Icons.message_outlined),
              label: "Hoá đơn",
            ),
            BottomNavigationBarItem(
              icon: Icon(Icons.more_horiz_outlined),
              label: "Khác",
            ),
          ],
          // press for switch tab
          onTap: (index) {
            setState(() {
              bottomBarIndex = index;
              if (index == 0) {
                runApp(Home());
              } else if (index == 1) {
                runApp(HistoryCounter());
              } else if (index == 2) {
                runApp(HistoryInvoice());
              } else if (index == 3) {
                runApp(More());
              }
            });
          },
        ),
      ),
    );
  }
}
