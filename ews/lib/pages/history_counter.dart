import 'package:ews/models/recordInfo.dart';
import 'package:ews/pages/colors.dart';
import 'package:ews/pages/counter_view_details.dart';
import 'package:ews/pages/history_invoice.dart';
import 'package:ews/resource/api_manager.dart';
import 'package:flutter/material.dart';

import 'Home.dart';
import 'more.dart';

class HistoryCounter extends StatefulWidget {
  @override
  _HistoryCounterState createState() => _HistoryCounterState();
}

class _HistoryCounterState extends State<HistoryCounter> {
  ScrollController _controller = new ScrollController();
  Future<RecordModel> _recordModel;
  String _value = "Điện";

  List<String> _values = new List<String>();

  String someText = "The Coffee House";

  @override
  // ignore: must_call_super
  void initState() {
    _recordModel = API_Manager().getRecord(106);
    _values.addAll(["Điện", "Nước"]);
    _values.elementAt(0);
  }

  void _onChanged(String value) {
    setState(() {
      _value = value;
    });
  }

  String getString(String str, int size) {
    String result = "";
    if (str.length >= size) {
      result = str.substring(0, (size - 3)) + " ...";
    } else {
      result = str;
    }
    return result;
  }

  Color borderColor = Colors.redAccent;
  Color btnColor = Colors.grey[300];

  int bottomBarIndex = 1;
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      theme: ThemeData(primaryColor: myPrimaryColor),
      home: Scaffold(
        // Header
        appBar: AppBar(
          toolbarHeight: 60,
          centerTitle: true,
          title: Text(
            "Lịch sử ghi công tơ",
            style: TextStyle(fontSize: 27, color: Colors.white),
          ),
        ),
        // Body
        body: Padding(
            padding: const EdgeInsets.all(20.0),
            child: SingleChildScrollView(
              child: Column(children: <Widget>[
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: <Widget>[
                    new Text(
                      "Loại công tơ : ",
                      style: TextStyle(
                        fontSize: 25,
                      ),
                    ),
                    new DropdownButton(
                      itemHeight: 50,
                      value: _value,
                      items: _values.map((String value) {
                        return DropdownMenuItem(
                          value: value,
                          child: new Row(
                            children: <Widget>[new Text("$value")],
                          ),
                        );
                      }).toList(),
                      onChanged: (String value) {
                        if (value.compareTo("Điện") == 0) {
                          borderColor = Colors.redAccent;
                          _recordModel = API_Manager().getRecord(106);
                        } else {
                          borderColor = Colors.blueAccent;
                          _recordModel = API_Manager().getRecord(107);
                        }
                        _onChanged(value);
                      },
                      style: TextStyle(fontSize: 25, color: Colors.black),
                    )
                  ],
                ),
                SizedBox(
                  height: 20,
                ),
                FutureBuilder<RecordModel>(
                    future: _recordModel,
                    builder: (context, snapshot) {
                      if (snapshot.hasData) {
                        return ListView.builder(
                            physics: const BouncingScrollPhysics(
                                parent: AlwaysScrollableScrollPhysics()),
                            shrinkWrap: true,
                            itemCount: snapshot.data.data.length,
                            itemBuilder: (context, index) {
                              var record = snapshot.data.data[index];
                              return InkWell(
                                onTap: () {
                                  if (_value.contains("Điện")) {
                                    runApp(new MaterialApp(
                                        home: new CounterViewDetails(
                                      id: record.id,
                                      category: 1,
                                    )));
                                  } else {
                                    runApp(new MaterialApp(
                                        home: new CounterViewDetails(
                                      id: record.id,
                                      category: 2,
                                    )));
                                  }
                                },
                                child: Container(
                                  margin: EdgeInsets.only(bottom: 20),
                                  decoration: BoxDecoration(
                                      border: Border.all(
                                    color: borderColor,
                                  )),
                                  child: Column(
                                    crossAxisAlignment:
                                        CrossAxisAlignment.center,
                                    children: <Widget>[
                                      SizedBox(
                                        height: 10,
                                      ),
                                      Row(
                                        mainAxisAlignment:
                                            MainAxisAlignment.start,
                                        children: <Widget>[
                                          Column(
                                            children: [
                                              SizedBox(
                                                width: 10,
                                              ),
                                            ],
                                          ),
                                          Column(
                                            children: <Widget>[
                                              Row(
                                                mainAxisAlignment:
                                                    MainAxisAlignment.start,
                                                children: <Widget>[
                                                  Column(
                                                    children: [
                                                      Text(
                                                        "Người ghi: ",
                                                        style: TextStyle(
                                                            fontSize: 16,
                                                            fontWeight:
                                                                FontWeight
                                                                    .bold),
                                                      ),
                                                    ],
                                                  ),
                                                  Column(
                                                    children: [
                                                      Text(
                                                        "${getString(record.createdTime.toString(), 13)}",
                                                        style: TextStyle(
                                                            fontSize: 16),
                                                      ),
                                                    ],
                                                  ),
                                                ],
                                              ),
                                              SizedBox(
                                                height: 10,
                                              ),
                                              Row(
                                                mainAxisAlignment:
                                                    MainAxisAlignment.start,
                                                children: [
                                                  Text(
                                                    "Tên cửa hàng: ",
                                                    style: TextStyle(
                                                        fontSize: 16,
                                                        fontWeight:
                                                            FontWeight.bold),
                                                  ),
                                                  Text(
                                                    "${getString("$someText", 27)}",
                                                    style:
                                                        TextStyle(fontSize: 16),
                                                  ),
                                                ],
                                              ),
                                              SizedBox(
                                                height: 10,
                                              ),
                                              Row(
                                                mainAxisAlignment:
                                                    MainAxisAlignment.start,
                                                children: [
                                                  Text(
                                                    "Số điện: ",
                                                    style: TextStyle(
                                                        fontSize: 16,
                                                        fontWeight:
                                                            FontWeight.bold),
                                                  ),
                                                  Text(
                                                    "${record.countNumber}",
                                                    style:
                                                        TextStyle(fontSize: 16),
                                                  ),
                                                ],
                                              ),
                                              SizedBox(
                                                height: 10,
                                              ),
                                            ],
                                          ),
                                        ],
                                      ),
                                    ],
                                  ),
                                ),
                              );
                            });
                      } else {
                        return Center(child: CircularProgressIndicator());
                      }
                    }),
              ]),
            )),
        // bottom bar
        bottomNavigationBar: BottomNavigationBar(
          backgroundColor: Colors.white,
          selectedItemColor: myPrimaryColor,
          unselectedItemColor: Colors.grey,
          currentIndex: bottomBarIndex,
          items: [
            BottomNavigationBarItem(
              icon: Icon(Icons.home_outlined),
              label: "Trang chủ",
            ),
            BottomNavigationBarItem(
              icon: Icon(Icons.history_outlined),
              label: "Công tơ",
            ),
            BottomNavigationBarItem(
              icon: Icon(Icons.message_outlined),
              label: "Hoá đơn",
            ),
            BottomNavigationBarItem(
              icon: Icon(Icons.more_horiz_outlined),
              label: "Khác",
            ),
          ],
          // press for switch tab
          onTap: (index) {
            setState(() {
              bottomBarIndex = index;
              if (index == 0) {
                runApp(Home());
              } else if (index == 1) {
                runApp(HistoryCounter());
              } else if (index == 2) {
                runApp(HistoryInvoice());
              } else if (index == 3) {
                runApp(More());
              }
            });
          },
        ),
      ),
    );
  }
}
