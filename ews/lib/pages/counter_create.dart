import 'package:ews/models/recordInfo.dart';
import 'package:ews/resource/api_manager.dart';
import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';

import 'colors.dart';
import 'history_counter.dart';
import 'history_invoice.dart';
import 'home.dart';
import 'more.dart';

class CounterCreate extends StatefulWidget {
  final String counterIndex;
  CounterCreate({Key key, @required this.counterIndex}) : super(key: key);

  @override
  _CounterCreateState createState() => _CounterCreateState();
}

class _CounterCreateState extends State<CounterCreate> {
  TextEditingController _counterIDController = TextEditingController();
  TextEditingController _countNumberController;
  @override
  void initState() {
    super.initState();
    _countNumberController = TextEditingController()
      ..text = widget.counterIndex;
  }

  Future<Record> _record;
  @override
  Widget build(BuildContext context) {
    int bottomBarIndex = 2;
    return Scaffold(
      appBar: (AppBar(
          toolbarHeight: 60,
          centerTitle: true,
          title: Text("Ghi công tơ",
              style: TextStyle(
                fontSize: 27,
                color: Colors.white,
              )))),
      body: SafeArea(
        child: Container(
          child: (_record == null)
              ? Padding(
                  padding: const EdgeInsets.all(20),
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                    children: [
                      Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: [
                          Column(
                            children: [
                              Container(
                                  alignment: AlignmentDirectional.centerStart,
                                  width: 150,
                                  height: 80,
                                  child: Text(
                                    "Mã công tơ:",
                                    style: TextStyle(
                                        fontSize: 20,
                                        fontWeight: FontWeight.bold,
                                        color: Colors.grey[700]),
                                  ))
                            ],
                          ),
                          Column(
                            children: [
                              Container(
                                  alignment: AlignmentDirectional.centerEnd,
                                  width: 300,
                                  height: 80,
                                  child: TextField(
                                    controller: _counterIDController,
                                    style: TextStyle(
                                        fontSize: 20,
                                        fontWeight: FontWeight.bold),
                                  ))
                            ],
                          )
                        ],
                      ),
                      Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: [
                          Column(
                            children: [
                              Container(
                                  alignment: AlignmentDirectional.centerStart,
                                  width: 150,
                                  height: 80,
                                  child: Text(
                                    "Số mới:",
                                    style: TextStyle(
                                        fontSize: 20,
                                        fontWeight: FontWeight.bold,
                                        color: Colors.grey[700]),
                                  ))
                            ],
                          ),
                          Column(
                            children: [
                              Container(
                                  alignment: AlignmentDirectional.centerEnd,
                                  width: 300,
                                  height: 80,
                                  child: TextField(
                                    controller: _countNumberController,
                                    style: TextStyle(
                                        fontSize: 20,
                                        fontWeight: FontWeight.bold),
                                  ))
                            ],
                          )
                        ],
                      ),
                      Row(
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: [
                          Container(
                            width: 200,
                            height: 35,
                            child: FlatButton(
                              height: 35,
                              minWidth: 150,
                              color: Colors.blueAccent[700],
                              shape: RoundedRectangleBorder(
                                borderRadius: BorderRadius.circular(0.0),
                                side: BorderSide(color: Colors.blueAccent[700]),
                              ),
                              onPressed: () {
                                setState(() {
                                  int counterNumber =
                                      int.parse(_countNumberController.text);
                                  int counterID =
                                      int.parse(_counterIDController.text);
                                  _record = API_Manager()
                                      .createRecord(counterNumber, counterID);
                                });
                              },
                              child: Text(
                                "Tạo mới",
                                style: TextStyle(
                                    color: Colors.white,
                                    fontSize: 20,
                                    fontWeight: FontWeight.bold),
                              ),
                            ),
                          ),
                          SizedBox(
                            width: 10,
                          ),
                          Container(
                            width: 200,
                            height: 35,
                            child: FlatButton(
                              height: 35,
                              minWidth: 150,
                              color: Colors.grey,
                              shape: RoundedRectangleBorder(
                                borderRadius: BorderRadius.circular(0.0),
                                side: BorderSide(color: Colors.grey),
                              ),
                              onPressed: () {
                                runApp(Home());
                              },
                            ),
                          )
                        ],
                      )
                    ],
                  ),
                )
              : FutureBuilder<Record>(
                  future: _record,
                  builder: (context, snapshot) {
                    if (snapshot.hasData) {
                      return Text('Ghi công tơ thành công');
                    } else if (snapshot.hasError) {
                      return Text("${snapshot.error}");
                    }
                    return CircularProgressIndicator();
                  },
                ),
        ),
      ),
      // bottom bar
      bottomNavigationBar: BottomNavigationBar(
        backgroundColor: Colors.white,
        selectedItemColor: myPrimaryColor,
        unselectedItemColor: Colors.grey,
        currentIndex: bottomBarIndex,
        items: [
          BottomNavigationBarItem(
            icon: Icon(Icons.home_outlined),
            label: "Trang chủ",
          ),
          BottomNavigationBarItem(
            icon: Icon(Icons.history_outlined),
            label: "Công tơ",
          ),
          BottomNavigationBarItem(
            icon: Icon(Icons.message_outlined),
            label: "Hoá đơn",
          ),
          BottomNavigationBarItem(
            icon: Icon(Icons.more_horiz_outlined),
            label: "Khác",
          ),
        ],
        // press for switch tab
        onTap: (index) {
          setState(() {
            bottomBarIndex = index;
            if (index == 0) {
              runApp(Home());
            } else if (index == 1) {
              runApp(HistoryCounter());
            } else if (index == 2) {
              runApp(HistoryInvoice());
            } else if (index == 3) {
              runApp(More());
            }
          });
        },
      ),
    );
  }
}
