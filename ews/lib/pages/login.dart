import 'package:ews/ultils/auth_helper.dart';
import 'package:flutter/material.dart';
import 'package:flutter_signin_button/flutter_signin_button.dart';

import 'colors.dart';

class LoginPage extends StatefulWidget {
  @override
  _LoginPageState createState() => _LoginPageState();
}

class _LoginPageState extends State<LoginPage> {
  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      home: Scaffold(
        // Header
        appBar: AppBar(
          toolbarHeight: 60,
          centerTitle: true,
          title: Text(
            "E W S",
            style: TextStyle(fontSize: 27, color: Colors.white),
          ),
        ),
        body: SingleChildScrollView(
          child: Padding(
            padding: const EdgeInsets.all(16.0),
            child: Center(
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: <Widget>[
                  Center(
                    child: Column(
                      children: [
                        Image.asset('lib/assets/images/logo0.png',
                            width: 200, height: 200),
                      ],
                    ),
                  ),
                  const SizedBox(height: 20.0),
                  Center(
                    child: Column(
                      children: [
                        Text(
                          "Electric and Water",
                          style: TextStyle(
                              fontWeight: FontWeight.bold,
                              fontSize: 30.0,),
                        ),
                        Text(
                          "Management System",
                          style: TextStyle(
                              fontWeight: FontWeight.bold,
                              fontSize: 30.0,),
                        ),
                      ],
                    ),
                  ),
                  const SizedBox(height: 20.0),
                  Center(
                    child: Column(
                      children: [
                        SignInButton(
                          Buttons.GoogleDark,
                          text: "Đăng nhập với Google",
                          onPressed: () async {
                            try {
                              await AuthHelper.signInWithGoogle();
                            } catch (e) {
                              print(e);
                            }
                          },
                        )
                      ],
                    ),
                  ),
                ],
              ),
            ),
          ),
        ),
      ),
    );
  }
}
