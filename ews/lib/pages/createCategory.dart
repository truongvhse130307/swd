import 'package:ews/models/categoryInfo.dart';
import 'package:ews/resource/api_manager.dart';
import 'package:flutter/material.dart';

class CreateCategory extends StatefulWidget {
  @override
  _CreateCategoryState createState() => _CreateCategoryState();
}

class _CreateCategoryState extends State<CreateCategory> {
  TextEditingController _idController = TextEditingController();
  TextEditingController _nameController = TextEditingController();

  Future<CategoryEW> _categoryEW;
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(title: Text("Create new Category")),
      body: Container(
        alignment: Alignment.center,
        padding: const EdgeInsets.all(8.0),
        child: (_categoryEW == null)
            ? Column(
                mainAxisAlignment: MainAxisAlignment.center,
                children: <Widget>[
                  TextField(
                    controller: _idController,
                    decoration: InputDecoration(hintText: 'Enter ID'),
                  ),
                  TextField(
                    controller: _nameController,
                    decoration: InputDecoration(hintText: 'Enter Name'),
                  ),
                  ElevatedButton(
                    child: Text('Create Data'),
                    onPressed: () {
                      setState(() {
                        int id = _idController.hashCode;
                        String name = _nameController.text;
                        DateTime createdTime = DateTime.now();
                        DateTime lastModified = DateTime.now();
                        _categoryEW = API_Manager().createCategoryEW(
                            id, name, createdTime, lastModified, false);
                      });
                    },
                  ),
                ],
              )
            : FutureBuilder<CategoryEW>(
                future: _categoryEW,
                builder: (context, snapshot) {
                  if (snapshot.hasData) {
                    return Text('Create complete');
                  } else if (snapshot.hasError) {
                    return Text("${snapshot.error}");
                  }
                  return CircularProgressIndicator();
                },
              ),
      ),
    );
  }
}
