import 'package:ews/main.dart';
import 'package:ews/pages/colors.dart';
import 'package:ews/pages/history_invoice.dart';
import 'package:ews/ultils/auth_helper.dart';
import 'package:flutter/material.dart';

import 'home.dart';
import 'history_counter.dart';

class More extends StatefulWidget {
  @override
  _MoreState createState() => _MoreState();
}

class _MoreState extends State<More> {
  Color borderColor = myPrimaryColor;
  Color btnColor = Colors.grey[300];

  int bottomBarIndex = 3;
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      theme: ThemeData(primaryColor: myPrimaryColor),
      home: Scaffold(
        // Header
        appBar: AppBar(
          toolbarHeight: 60,
          centerTitle: true,
          title: Text(
            "Các tính năng khác",
            style: TextStyle(fontSize: 27, color: Colors.white),
          ),
        ),
        // Body
        body: Padding(
          padding: const EdgeInsets.all(20.0),
          child: ListView(
            children: <Widget>[
              Column(
                children: <Widget>[
                  Row(
                    mainAxisAlignment: MainAxisAlignment.spaceAround,
                    children: <Widget>[
                      Container(
                        child: ButtonTheme(
                          minWidth: 300.0,
                          height: 50,
                          child: RaisedButton(
                            padding: const EdgeInsets.all(10.0),
                            child: Text(
                              "Đăng xuất",
                              style: TextStyle(
                                fontSize: 25,
                              ),
                            ),
                            onPressed: () {
                              AuthHelper.logOut();
                              runApp(MyApp());
                            },
                            color: btnColor,
                          ),
                        ),
                      ),
                    ],
                  ),
                  SizedBox(
                    height: 20,
                  ),
                ],
              ),
            ],
          ),
        ),

        // bottom bar
        bottomNavigationBar: BottomNavigationBar(
          backgroundColor: Colors.white,
          selectedItemColor: myPrimaryColor,
          unselectedItemColor: Colors.grey,
          currentIndex: bottomBarIndex,
          items: [
            BottomNavigationBarItem(
              icon: Icon(Icons.home_outlined),
              label: "Trang chủ",
            ),
            BottomNavigationBarItem(
              icon: Icon(Icons.history_outlined),
              label: "Công tơ",
            ),
            BottomNavigationBarItem(
              icon: Icon(Icons.message_outlined),
              label: "Hoá đơn",
            ),
            BottomNavigationBarItem(
              icon: Icon(Icons.more_horiz_outlined),
              label: "Khác",
            ),
          ],
          // press for switch tab
          onTap: (index) {
            setState(() {
              bottomBarIndex = index;
              if (index == 0) {
                runApp(Home());
              } else if (index == 1) {
                runApp(HistoryCounter());
              } else if (index == 2) {
                runApp(HistoryInvoice());
              } else if (index == 3) {
                runApp(More());
              }
            });
          },
        ),
      ),
    );
  }
}
