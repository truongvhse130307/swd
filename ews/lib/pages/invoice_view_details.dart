import 'package:ews/models/invoiceInfo.dart';
import 'package:ews/resource/api_manager.dart';
import 'package:flutter/material.dart';

import 'colors.dart';
import 'history_counter.dart';
import 'history_invoice.dart';
import 'home.dart';
import 'more.dart';

class InvoiceViewDetails extends StatefulWidget {
  @override
  _InvoiceViewDetails createState() => _InvoiceViewDetails();
  final int id;
  final int category;
  InvoiceViewDetails({Key key, @required this.id, @required this.category})
      : super(key: key);
}

Future<Invoice> _invoice;

class _InvoiceViewDetails extends State<InvoiceViewDetails> {
  String _value = "Điện";
  @override
  void initState() {
    super.initState();
    _invoice = API_Manager().getInvoiceByID(widget.id);
    if (widget.category == 1) {
      _value = "Điện";
    } else {
      _value = "Nước";
    }
  }

  @override
  Widget build(BuildContext context) {
    int bottomBarIndex = 2;
    return MaterialApp(
      home: Scaffold(
        appBar: (AppBar(
            toolbarHeight: 60,
            centerTitle: true,
            title: Text("Chi tiết hoá đơn",
                style: TextStyle(
                  fontSize: 27,
                  color: Colors.white,
                )))),
        body: SafeArea(
          child: ListView(
            padding: EdgeInsets.symmetric(vertical: 15.0),
            children: <Widget>[
              // Details(),
              Padding(
                padding: const EdgeInsets.all(20.0),
                child: SingleChildScrollView(
                  child: Column(
                    children: [
                      FutureBuilder<Invoice>(
                          future: _invoice,
                          builder: (context, snapshot) {
                            if (snapshot.hasData) {
                              return ListView.builder(
                                  shrinkWrap: true,
                                  itemCount: 1,
                                  itemBuilder: (context, index) {
                                    var invoice = snapshot.data;
                                    return Column(children: [
                                      Row(
                                        mainAxisAlignment:
                                            MainAxisAlignment.spaceBetween,
                                        children: <Widget>[
                                          Column(
                                            children: [
                                              Text(
                                                "Mã hoá đơn:",
                                                style: TextStyle(
                                                    fontSize: 18,
                                                    fontWeight:
                                                        FontWeight.bold),
                                              ),
                                            ],
                                          ),
                                          Column(
                                            children: [
                                              Text("${invoice.id}",
                                                  style:
                                                      TextStyle(fontSize: 18)),
                                            ],
                                          )
                                        ],
                                      ),
                                      SizedBox(
                                        height: 20,
                                      ),
                                      Row(
                                        mainAxisAlignment:
                                            MainAxisAlignment.spaceBetween,
                                        children: <Widget>[
                                          Text(
                                            "Tên cửa hàng:",
                                            style: TextStyle(
                                                fontSize: 18,
                                                fontWeight: FontWeight.bold),
                                          ),
                                          Text("The Coffee House",
                                              style: TextStyle(fontSize: 18))
                                        ],
                                      ),
                                      SizedBox(
                                        height: 20,
                                      ),
                                      Row(
                                        mainAxisAlignment:
                                            MainAxisAlignment.spaceBetween,
                                        children: <Widget>[
                                          Text(
                                            "Loại hoá đơn:",
                                            style: TextStyle(
                                                fontSize: 18,
                                                fontWeight: FontWeight.bold),
                                          ),
                                          Text(_value,
                                              style: TextStyle(fontSize: 18))
                                        ],
                                      ),
                                      SizedBox(
                                        height: 20,
                                      ),
                                      Row(
                                        mainAxisAlignment:
                                            MainAxisAlignment.spaceBetween,
                                        children: <Widget>[
                                          Text(
                                            "Từ ngày:",
                                            style: TextStyle(
                                                fontSize: 18,
                                                fontWeight: FontWeight.bold),
                                          ),
                                          Text("${invoice.startDate}",
                                              style: TextStyle(fontSize: 18))
                                        ],
                                      ),
                                      SizedBox(
                                        height: 20,
                                      ),
                                      Row(
                                        mainAxisAlignment:
                                            MainAxisAlignment.spaceBetween,
                                        children: <Widget>[
                                          Text(
                                            "Đến ngày:",
                                            style: TextStyle(
                                                fontSize: 18,
                                                fontWeight: FontWeight.bold),
                                          ),
                                          Text("${invoice.endDate}",
                                              style: TextStyle(fontSize: 18))
                                        ],
                                      ),
                                      SizedBox(
                                        height: 20,
                                      ),
                                      Row(
                                        mainAxisAlignment:
                                            MainAxisAlignment.spaceBetween,
                                        children: <Widget>[
                                          Text(
                                            "Số công tơ:",
                                            style: TextStyle(
                                                fontSize: 18,
                                                fontWeight: FontWeight.bold),
                                          ),
                                          Text("${invoice.countIndex}",
                                              style: TextStyle(fontSize: 18))
                                        ],
                                      ),
                                      SizedBox(
                                        height: 20,
                                      ),
                                      Row(
                                        mainAxisAlignment:
                                            MainAxisAlignment.spaceBetween,
                                        children: <Widget>[
                                          Text(
                                            "Tổng tiền:",
                                            style: TextStyle(
                                                fontSize: 18,
                                                fontWeight: FontWeight.bold),
                                          ),
                                          Text("${invoice.cost} VNĐ",
                                              style: TextStyle(fontSize: 18))
                                        ],
                                      ),
                                      SizedBox(
                                        height: 20,
                                      ),
                                      Row(
                                        mainAxisAlignment:
                                            MainAxisAlignment.spaceBetween,
                                        children: <Widget>[
                                          Text(
                                            "Ngày tạo:",
                                            style: TextStyle(
                                                fontSize: 18,
                                                fontWeight: FontWeight.bold),
                                          ),
                                          Text("${invoice.createdTime}",
                                              style: TextStyle(fontSize: 18))
                                        ],
                                      ),
                                      SizedBox(
                                        height: 20,
                                      ),
                                    ]);
                                  });
                            } else {
                              return Center(child: CircularProgressIndicator());
                            }
                          })
                    ],
                  ),
                ),
              )
            ],
          ),
        ),
        // bottom bar
        bottomNavigationBar: BottomNavigationBar(
          backgroundColor: Colors.white,
          selectedItemColor: myPrimaryColor,
          unselectedItemColor: Colors.grey,
          currentIndex: bottomBarIndex,
          items: [
            BottomNavigationBarItem(
              icon: Icon(Icons.home_outlined),
              label: "Trang chủ",
            ),
            BottomNavigationBarItem(
              icon: Icon(Icons.history_outlined),
              label: "Công tơ",
            ),
            BottomNavigationBarItem(
              icon: Icon(Icons.message_outlined),
              label: "Hoá đơn",
            ),
            BottomNavigationBarItem(
              icon: Icon(Icons.more_horiz_outlined),
              label: "Khác",
            ),
          ],
          // press for switch tab
          onTap: (index) {
            setState(() {
              bottomBarIndex = index;
              if (index == 0) {
                runApp(Home());
              } else if (index == 1) {
                runApp(HistoryCounter());
              } else if (index == 2) {
                runApp(HistoryInvoice());
              } else if (index == 3) {
                runApp(More());
              }
            });
          },
        ),
      ),
    );
  }
}
