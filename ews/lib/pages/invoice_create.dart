import 'package:ews/models/invoiceInfo.dart';
import 'package:ews/resource/api_manager.dart';
import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';

import 'colors.dart';
import 'history_counter.dart';
import 'history_invoice.dart';
import 'home.dart';
import 'more.dart';

class InvoiceCreate extends StatefulWidget {
  InvoiceCreate({Key key}) : super(key: key);

  @override
  _InvoiceCreateState createState() => _InvoiceCreateState();
}

class _InvoiceCreateState extends State<InvoiceCreate> {
  @override
  Widget build(BuildContext context) {
    int bottomBarIndex = 2;
    return Scaffold(
      appBar: (AppBar(
          toolbarHeight: 60,
          centerTitle: true,
          title: Text("Ghi công tơ",
              style: TextStyle(
                fontSize: 27,
                color: Colors.white,
              )))),
      body: SafeArea(
        child: Details(),
      ),
      // bottom bar
      bottomNavigationBar: BottomNavigationBar(
        backgroundColor: Colors.white,
        selectedItemColor: myPrimaryColor,
        unselectedItemColor: Colors.grey,
        currentIndex: bottomBarIndex,
        items: [
          BottomNavigationBarItem(
            icon: Icon(Icons.home_outlined),
            label: "Trang chủ",
          ),
          BottomNavigationBarItem(
            icon: Icon(Icons.history_outlined),
            label: "Công tơ",
          ),
          BottomNavigationBarItem(
            icon: Icon(Icons.message_outlined),
            label: "Hoá đơn",
          ),
          BottomNavigationBarItem(
            icon: Icon(Icons.more_horiz_outlined),
            label: "Khác",
          ),
        ],
        // press for switch tab
        onTap: (index) {
          setState(() {
            bottomBarIndex = index;
            if (index == 0) {
              runApp(Home());
            } else if (index == 1) {
              runApp(HistoryCounter());
            } else if (index == 2) {
              runApp(HistoryInvoice());
            } else if (index == 3) {
              runApp(More());
            }
          });
        },
      ),
    );
  }
}

class DropDown extends StatefulWidget {
  @override
  _DropDownState createState() => _DropDownState();
}

class _DropDownState extends State<DropDown> {
  String dropdownValue = 'Điện';
  var _invoiceType = ['Điện', 'Nước'];

  @override
  Widget build(BuildContext context) {
    return DropdownButton<String>(
      value: dropdownValue,
      icon: Icon(Icons.arrow_downward),
      iconSize: 24,
      elevation: 16,
      style: TextStyle(fontSize: 20, color: Colors.deepPurple),
      underline: Container(
        decoration: BoxDecoration(
            color: Colors.grey,
            border: Border.all(
                width: 0.5,
                color: Colors.blueAccent,
                style: BorderStyle.solid)),
      ),
      onChanged: (String newValue) {
        setState(() {
          dropdownValue = newValue;
        });
      },
      items: _invoiceType.map((String dropDownStringItem) {
        return DropdownMenuItem<String>(
          value: dropDownStringItem,
          child: Text(dropDownStringItem),
        );
      }).toList(),
    );
  }
}

class Details extends StatefulWidget {
  @override
  _Details createState() => _Details();
}

class _Details extends State<Details> {
  TextEditingController _counterNumberController = TextEditingController();
  TextEditingController _counterIDController = TextEditingController();
  TextEditingController _costController = TextEditingController();
  Future<Invoice> _invoice;

  @override
  Widget build(BuildContext context) {
    return Container(
      child: (_invoice == null)
          ? Padding(
              padding: const EdgeInsets.all(20),
              child: SingleChildScrollView(
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                  children: [
                    Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        Column(
                          children: [
                            Container(
                                alignment: AlignmentDirectional.centerStart,
                                width: 150,
                                height: 80,
                                child: Text(
                                  "Mã công tơ:",
                                  style: TextStyle(
                                      fontSize: 20,
                                      fontWeight: FontWeight.bold,
                                      color: Colors.grey[700]),
                                ))
                          ],
                        ),
                        Column(
                          children: <Widget>[
                            Container(
                              alignment: AlignmentDirectional.centerEnd,
                              width: 170,
                              height: 80,
                              child: TextField(
                                controller: _counterIDController,
                              ),
                            )
                          ],
                        )
                      ],
                    ),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        Column(
                          children: [
                            Container(
                                alignment: AlignmentDirectional.centerStart,
                                width: 150,
                                height: 80,
                                child: Text(
                                  "Số công tơ:",
                                  style: TextStyle(
                                      fontSize: 20,
                                      fontWeight: FontWeight.bold,
                                      color: Colors.grey[700]),
                                ))
                          ],
                        ),
                        Column(
                          children: <Widget>[
                            Container(
                                alignment: AlignmentDirectional.centerEnd,
                                width: 170,
                                height: 80,
                                child: TextField(
                                  controller: _counterNumberController,
                                ))
                          ],
                        )
                      ],
                    ),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        Column(
                          children: [
                            Container(
                                alignment: AlignmentDirectional.centerStart,
                                width: 150,
                                height: 80,
                                child: Text(
                                  "Tổng tiền:",
                                  style: TextStyle(
                                      fontSize: 20,
                                      fontWeight: FontWeight.bold,
                                      color: Colors.grey[700]),
                                ))
                          ],
                        ),
                        Column(
                          children: <Widget>[
                            Container(
                                alignment: AlignmentDirectional.centerEnd,
                                width: 170,
                                height: 80,
                                child: TextField(
                                  controller: _costController,
                                ))
                          ],
                        )
                      ],
                    ),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: [
                        Container(
                          width: 200,
                          height: 35,
                          child: FlatButton(
                            height: 35,
                            minWidth: 150,
                            color: Colors.blueAccent[700],
                            shape: RoundedRectangleBorder(
                              borderRadius: BorderRadius.circular(0.0),
                              side: BorderSide(color: Colors.blueAccent[700]),
                            ),
                            onPressed: () {
                              setState(() {
                                int counterID =
                                    int.parse(_counterIDController.text);
                                int counterIndex =
                                    int.parse(_counterNumberController.text);
                                int cost = int.parse(_costController.text);
                                _invoice = API_Manager().createInvoice(
                                    cost, counterID, counterIndex);
                              });
                            },
                            child: Text(
                              "Tạo mới",
                              style: TextStyle(
                                  color: Colors.white,
                                  fontSize: 20,
                                  fontWeight: FontWeight.bold),
                            ),
                          ),
                        ),
                        SizedBox(
                          width: 10,
                        ),
                      ],
                    )
                  ],
                ),
              ),
            )
          : FutureBuilder<Invoice>(
              future: _invoice,
              builder: (context, snapshot) {
                if (snapshot.hasData) {
                  return Text('Ghi hóa đơn thành công');
                } else if (snapshot.hasError) {
                  return Text("${snapshot.error}");
                }
                return CircularProgressIndicator();
              },
            ),
    );
  }
}
