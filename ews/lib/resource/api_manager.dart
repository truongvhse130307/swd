import 'dart:convert';
import 'dart:developer';

import 'package:ews/constants/strings.dart';
import 'package:ews/models/categoryInfo.dart';
import 'package:ews/models/counterInfo.dart';
import 'package:ews/models/invoiceInfo.dart';
import 'package:ews/models/recordInfo.dart';
import 'package:ews/models/storeInfo.dart';
import 'package:http/http.dart' as http;
import 'package:shared_preferences/shared_preferences.dart';

class API_Manager {
  // Future<BrandModel> getBrand() async {
  //   var client = http.Client();
  //   var brandModel = null;
  //   try {
  //     var response = await client.get(Strings.brand_url);
  //     if (response.statusCode == 200) {
  //       var jsonString = response.body;
  //       var jsonMap = json.decode(jsonString);
  //       brandModel = BrandModel.fromJson(jsonMap);
  //     }
  //   } catch (Exception) {
  //     return brandModel;
  //   }
  //   return brandModel;
  // }

  Future<CategoryModel> getCategory() async {
    var client = http.Client();
    var categoryModel = null;
    try {
      var response = await client.get(Strings.category_url);
      if (response.statusCode == 200) {
        var jsonString = response.body;
        var jsonMap = json.decode(jsonString);
        categoryModel = CategoryModel.fromJson(jsonMap);
      }
    } catch (Exception) {
      return categoryModel;
    }
    return categoryModel;
  }

  Future<CategoryEW> createCategoryEW(int id, String name, DateTime createdTime,
      DateTime lastModified, bool isDisable) async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    http.Response response = await http.post(
      Strings.category_url,
      headers: <String, String>{
        'Content-Type': 'application/json; charset=UTF-8',
        'Authorization': 'Bearer ' + prefs.getString('ACCESS_TOKEN'),
      },
      body: jsonEncode(<String, dynamic>{'name': name, 'isDisable': isDisable}),
    );
    if (response.statusCode == 201) {
      return CategoryEW.fromJson(jsonDecode(response.body));
    }
    if (response.statusCode == 415) {
      throw Exception('Unsupport Media type');
    } else {
      var status = response.statusCode;
      throw Exception('$status');
    }
  }

  Future<CounterModel> getCounter() async {
    var client = http.Client();
    var counterModel = null;
    try {
      var response = await client.get(Strings.counter_url);
      if (response.statusCode == 200) {
        var jsonString = response.body;
        var jsonMap = json.decode(jsonString);
        counterModel = CounterModel.fromJson(jsonMap);
      }
    } catch (Exception) {
      return counterModel;
    }
    return counterModel;
  }

  // Future<DeviceModel> getDevice() async {
  //   var client = http.Client();
  //   var deviceModel = null;
  //   try {
  //     var response = await client.get(Strings.device_url);
  //     if (response.statusCode == 200) {
  //       var jsonString = response.body;
  //       var jsonMap = json.decode(jsonString);
  //       deviceModel = DeviceModel.fromJson(jsonMap);
  //     }
  //   } catch (Exception) {
  //     return deviceModel;
  //   }
  //   return deviceModel;
  // }

  // Future<DeviceTypeModel> getDeviceType() async {
  //   var client = http.Client();
  //   var deviceTypeModel = null;
  //   try {
  //     var response = await client.get(Strings.deviceType_url);
  //     if (response.statusCode == 200) {
  //       var jsonString = response.body;
  //       var jsonMap = json.decode(jsonString);
  //       deviceTypeModel = DeviceTypeModel.fromJson(jsonMap);
  //     }
  //   } catch (Exception) {
  //     return deviceTypeModel;
  //   }
  //   return deviceTypeModel;
  // }

  // Future<EmployeeStoreDetailModel> getEmployeeStoreDetail() async {
  //   var client = http.Client();
  //   var employeeStoreDetailsModel = null;
  //   try {
  //     var response = await client.get(Strings.employeeStoreDetails_url);
  //     if (response.statusCode == 200) {
  //       var jsonString = response.body;
  //       var jsonMap = json.decode(jsonString);
  //       employeeStoreDetailsModel = EmployeeStoreDetailModel.fromJson(jsonMap);
  //     }
  //   } catch (Exception) {
  //     return employeeStoreDetailsModel;
  //   }
  //   return employeeStoreDetailsModel;
  // }

  Future<InvoiceModel> getInvoice(int id) async {
    var client = http.Client();
    var invoiceModel = null;
    try {
      var response =
          await client.get(Strings.invoice_url + "?Id=$id&field=counter");
      if (response.statusCode == 200) {
        var jsonString = response.body;
        var jsonMap = json.decode(jsonString);
        invoiceModel = InvoiceModel.fromJson(jsonMap);
      }
    } catch (Exception) {
      return invoiceModel;
    }
    return invoiceModel;
  }

  Future<Invoice> getInvoiceByID(int id) async {
    var client = http.Client();
    var invoiceModel = null;
    var response = await client.get(Strings.invoice_url + '/$id');
    if (response.statusCode == 200) {
      var jsonString = response.body;
      var jsonMap = jsonDecode(jsonString);
      invoiceModel = Invoice.fromJson(jsonMap);
      return invoiceModel;
    } else {
      throw Exception('Failed to load Data');
    }
  }

  Future<Invoice> createInvoice(
      int cost, int counterID, int counterIndex) async {
    http.Response response = await http.post(
      Strings.invoice_url + '?counterId=$counterID',
      headers: <String, String>{
        'Content-Type': 'application/json; charset=UTF-8',
      },
      body: jsonEncode(<String, dynamic>{
        'cost': cost,
        'countIndex': counterIndex,
        'createdTime': DateTime.now().toIso8601String(),
        'lastModified': DateTime.now().toIso8601String()
      }),
    );
    if (response.statusCode == 201) {
      return Invoice.fromJson(jsonDecode(response.body));
    }
    if (response.statusCode == 415) {
      throw Exception('Unsupport Media type');
    } else {
      var status = response.statusCode;
      throw Exception('$status');
    }
  }

  Future<RecordModel> getRecord(int id) async {
    var client = http.Client();
    var recordModel = null;
    try {
      var response =
          await client.get(Strings.record_url + "?Id=$id&field=counter");
      if (response.statusCode == 200) {
        var jsonString = response.body;
        var jsonMap = json.decode(jsonString);
        recordModel = RecordModel.fromJson(jsonMap);
      }
    } catch (Exception) {
      return recordModel;
    }
    return recordModel;
  }

  Future<Record> getRecordByID(int id) async {
    var client = http.Client();
    var record = null;
    var response = await client.get(Strings.record_url + '/$id');
    if (response.statusCode == 200) {
      var jsonString = response.body;
      var jsonMap = jsonDecode(jsonString);
      record = Record.fromJson(jsonMap);
      return record;
    } else {
      throw Exception('Failed to load Data');
    }
  }

  Future<Record> createRecord(int counterNumber, int counterID) async {
    http.Response response = await http.post(
      Strings.record_url + '?counterId=$counterID',
      headers: <String, String>{
        'Content-Type': 'application/json; charset=UTF-8',
      },
      body: jsonEncode(<String, dynamic>{
        'countNumber': counterNumber,
        'createdTime': DateTime.now().toIso8601String(),
        'lastModified': DateTime.now().toIso8601String()
      }),
    );
    if (response.statusCode == 201) {
      return Record.fromJson(jsonDecode(response.body));
    }
    if (response.statusCode == 415) {
      throw Exception('Unsupport Media type');
    } else {
      var status = response.statusCode;
      throw Exception('$status');
    }
  }
  // Future<RoleModel> getRole() async {
  //   var client = http.Client();
  //   var roleModel = null;
  //   try {
  //     var response = await client.get(Strings.role_url);
  //     if (response.statusCode == 200) {
  //       var jsonString = response.body;
  //       var jsonMap = json.decode(jsonString);
  //       roleModel = RoleModel.fromJson(jsonMap);
  //     }
  //   } catch (Exception) {
  //     return roleModel;
  //   }
  //   return roleModel;
  // }

  Future<StoreModel> getStore() async {
    var client = http.Client();
    var storeModel = null;
    try {
      var response = await client.get(Strings.store_url);
      if (response.statusCode == 200) {
        var jsonString = response.body;
        var jsonMap = json.decode(jsonString);
        storeModel = StoreModel.fromJson(jsonMap);
      }
    } catch (Exception) {
      return storeModel;
    }
    return storeModel;
  }

  Future<StoreModel> createStore(Store store) async {
    var client = http.Client();
  }

  // Future<UserModel> getUser() async {
  //   var client = http.Client();
  //   var userModel = null;
  //   try {
  //     var response = await client.get(Strings.user_url);
  //     if (response.statusCode == 200) {
  //       var jsonString = response.body;
  //       var jsonMap = json.decode(jsonString);
  //       userModel = UserModel.fromJson(jsonMap);
  //     }
  //   } catch (Exception) {
  //     return userModel;
  //   }
  //   return userModel;
  // }
}
