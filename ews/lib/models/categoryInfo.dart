// To parse this JSON data, do
//
//     final categoryModel = categoryModelFromJson(jsonString);

import 'dart:convert';

CategoryModel categoryModelFromJson(String str) =>
    CategoryModel.fromJson(json.decode(str));

String categoryModelToJson(CategoryModel data) => json.encode(data.toJson());

class CategoryModel {
  CategoryModel({
    this.pageNumber,
    this.pageSize,
    this.firstPage,
    this.lastPage,
    this.totalPages,
    this.totalRecords,
    this.nextPage,
    this.previousPage,
    this.data,
    this.succeeded,
    this.errors,
    this.message,
  });

  int pageNumber;
  int pageSize;
  String firstPage;
  String lastPage;
  int totalPages;
  int totalRecords;
  String nextPage;
  dynamic previousPage;
  List<CategoryEW> data;
  bool succeeded;
  dynamic errors;
  dynamic message;

  factory CategoryModel.fromJson(Map<String, dynamic> json) => CategoryModel(
        pageNumber: json["pageNumber"],
        pageSize: json["pageSize"],
        firstPage: json["firstPage"],
        lastPage: json["lastPage"],
        totalPages: json["totalPages"],
        totalRecords: json["totalRecords"],
        nextPage: json["nextPage"],
        previousPage: json["previousPage"],
        data: List<CategoryEW>.from(
            json["data"].map((x) => CategoryEW.fromJson(x))),
        succeeded: json["succeeded"],
        errors: json["errors"],
        message: json["message"],
      );

  Map<String, dynamic> toJson() => {
        "pageNumber": pageNumber,
        "pageSize": pageSize,
        "firstPage": firstPage,
        "lastPage": lastPage,
        "totalPages": totalPages,
        "totalRecords": totalRecords,
        "nextPage": nextPage,
        "previousPage": previousPage,
        "data": List<dynamic>.from(data.map((x) => x.toJson())),
        "succeeded": succeeded,
        "errors": errors,
        "message": message,
      };
}

class CategoryEW {
  CategoryEW({
    this.id,
    this.name,
    this.createdTime,
    this.lastModified,
    this.isDisable,
    this.counters,
    this.invoices,
  });

  int id;
  String name;
  DateTime createdTime;
  DateTime lastModified;
  bool isDisable;
  dynamic counters;
  dynamic invoices;

  factory CategoryEW.fromJson(Map<String, dynamic> json) => CategoryEW(
        id: json["id"],
        name: json["name"],
        createdTime: DateTime.parse(json["createdTime"]),
        lastModified: DateTime.parse(json["lastModified"]),
        isDisable: json["isDisable"],
        counters: json["counters"],
        invoices: json["invoices"],
      );

  Map<String, dynamic> toJson() => {
        "id": id,
        "name": name,
        "createdTime": createdTime.toIso8601String(),
        "lastModified": lastModified.toIso8601String(),
        "isDisable": isDisable,
        "counters": counters,
        "invoices": invoices,
      };
}
