// To parse this JSON data, do
//
//     final storeModel = storeModelFromJson(jsonString);

import 'dart:convert';

StoreModel storeModelFromJson(String str) =>
    StoreModel.fromJson(json.decode(str));

String storeModelToJson(StoreModel data) => json.encode(data.toJson());

class StoreModel {
  StoreModel({
    this.pageNumber,
    this.pageSize,
    this.firstPage,
    this.lastPage,
    this.totalPages,
    this.totalRecords,
    this.nextPage,
    this.previousPage,
    this.data,
    this.succeeded,
    this.errors,
    this.message,
  });

  int pageNumber;
  int pageSize;
  String firstPage;
  String lastPage;
  int totalPages;
  int totalRecords;
  String nextPage;
  dynamic previousPage;
  List<Store> data;
  bool succeeded;
  dynamic errors;
  dynamic message;

  factory StoreModel.fromJson(Map<String, dynamic> json) => StoreModel(
        pageNumber: json["pageNumber"],
        pageSize: json["pageSize"],
        firstPage: json["firstPage"],
        lastPage: json["lastPage"],
        totalPages: json["totalPages"],
        totalRecords: json["totalRecords"],
        nextPage: json["nextPage"],
        previousPage: json["previousPage"],
        data: List<Store>.from(json["data"].map((x) => Store.fromJson(x))),
        succeeded: json["succeeded"],
        errors: json["errors"],
        message: json["message"],
      );

  Map<String, dynamic> toJson() => {
        "pageNumber": pageNumber,
        "pageSize": pageSize,
        "firstPage": firstPage,
        "lastPage": lastPage,
        "totalPages": totalPages,
        "totalRecords": totalRecords,
        "nextPage": nextPage,
        "previousPage": previousPage,
        "data": List<dynamic>.from(data.map((x) => x.toJson())),
        "succeeded": succeeded,
        "errors": errors,
        "message": message,
      };
}

class Store {
  Store({
    this.id,
    this.address,
    this.createdTime,
    this.lastModified,
    this.isDisable,
    this.employeeStoreDetails,
    this.counters,
  });

  int id;
  String address;
  DateTime createdTime;
  DateTime lastModified;
  bool isDisable;
  dynamic employeeStoreDetails;
  dynamic counters;

  factory Store.fromJson(Map<String, dynamic> json) => Store(
        id: json["id"],
        address: json["address"],
        createdTime: DateTime.parse(json["createdTime"]),
        lastModified: DateTime.parse(json["lastModified"]),
        isDisable: json["isDisable"],
        employeeStoreDetails: json["employeeStoreDetails"],
        counters: json["counters"],
      );

  Map<String, dynamic> toJson() => {
        "id": id,
        "address": address,
        "createdTime": createdTime.toIso8601String(),
        "lastModified": lastModified.toIso8601String(),
        "isDisable": isDisable,
        "employeeStoreDetails": employeeStoreDetails,
        "counters": counters,
      };
}
