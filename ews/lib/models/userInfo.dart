// To parse this JSON data, do
//
//     final userModel = userModelFromJson(jsonString);

import 'dart:convert';

UserModel userModelFromJson(String str) => UserModel.fromJson(json.decode(str));

String userModelToJson(UserModel data) => json.encode(data.toJson());

class UserModel {
  UserModel({
    this.pageNumber,
    this.pageSize,
    this.firstPage,
    this.lastPage,
    this.totalPages,
    this.totalRecords,
    this.nextPage,
    this.previousPage,
    this.data,
    this.succeeded,
    this.errors,
    this.message,
  });

  int pageNumber;
  int pageSize;
  String firstPage;
  String lastPage;
  int totalPages;
  int totalRecords;
  String nextPage;
  dynamic previousPage;
  List<User> data;
  bool succeeded;
  dynamic errors;
  dynamic message;

  factory UserModel.fromJson(Map<String, dynamic> json) => UserModel(
        pageNumber: json["pageNumber"],
        pageSize: json["pageSize"],
        firstPage: json["firstPage"],
        lastPage: json["lastPage"],
        totalPages: json["totalPages"],
        totalRecords: json["totalRecords"],
        nextPage: json["nextPage"],
        previousPage: json["previousPage"],
        data: List<User>.from(json["data"].map((x) => User.fromJson(x))),
        succeeded: json["succeeded"],
        errors: json["errors"],
        message: json["message"],
      );

  Map<String, dynamic> toJson() => {
        "pageNumber": pageNumber,
        "pageSize": pageSize,
        "firstPage": firstPage,
        "lastPage": lastPage,
        "totalPages": totalPages,
        "totalRecords": totalRecords,
        "nextPage": nextPage,
        "previousPage": previousPage,
        "data": List<dynamic>.from(data.map((x) => x.toJson())),
        "succeeded": succeeded,
        "errors": errors,
        "message": message,
      };
}

class User {
  User({
    this.id,
    this.fullName,
    this.email,
    this.password,
    this.phoneNumber,
    this.isDisable,
    this.brands,
    this.counters,
    this.invoices,
    this.records,
    this.employeeStoreDetails,
  });

  int id;
  String fullName;
  String email;
  String password;
  String phoneNumber;
  bool isDisable;
  dynamic brands;
  dynamic counters;
  dynamic invoices;
  dynamic records;
  dynamic employeeStoreDetails;

  factory User.fromJson(Map<String, dynamic> json) => User(
        id: json["id"],
        fullName: json["fullName"],
        email: json["email"],
        password: json["password"],
        phoneNumber: json["phoneNumber"],
        isDisable: json["isDisable"],
        brands: json["brands"],
        counters: json["counters"],
        invoices: json["invoices"],
        records: json["records"],
        employeeStoreDetails: json["employeeStoreDetails"],
      );

  Map<String, dynamic> toJson() => {
        "id": id,
        "fullName": fullName,
        "email": email,
        "password": password,
        "phoneNumber": phoneNumber,
        "isDisable": isDisable,
        "brands": brands,
        "counters": counters,
        "invoices": invoices,
        "records": records,
        "employeeStoreDetails": employeeStoreDetails,
      };
}
