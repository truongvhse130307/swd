// To parse this JSON data, do
//
//     final deviceTypeModel = deviceTypeModelFromJson(jsonString);

import 'dart:convert';

DeviceTypeModel deviceTypeModelFromJson(String str) =>
    DeviceTypeModel.fromJson(json.decode(str));

String deviceTypeModelToJson(DeviceTypeModel data) =>
    json.encode(data.toJson());

class DeviceTypeModel {
  DeviceTypeModel({
    this.pageNumber,
    this.pageSize,
    this.firstPage,
    this.lastPage,
    this.totalPages,
    this.totalRecords,
    this.nextPage,
    this.previousPage,
    this.data,
    this.succeeded,
    this.errors,
    this.message,
  });

  int pageNumber;
  int pageSize;
  String firstPage;
  String lastPage;
  int totalPages;
  int totalRecords;
  String nextPage;
  dynamic previousPage;
  List<DeviceType> data;
  bool succeeded;
  dynamic errors;
  dynamic message;

  factory DeviceTypeModel.fromJson(Map<String, dynamic> json) =>
      DeviceTypeModel(
        pageNumber: json["pageNumber"],
        pageSize: json["pageSize"],
        firstPage: json["firstPage"],
        lastPage: json["lastPage"],
        totalPages: json["totalPages"],
        totalRecords: json["totalRecords"],
        nextPage: json["nextPage"],
        previousPage: json["previousPage"],
        data: List<DeviceType>.from(
            json["data"].map((x) => DeviceType.fromJson(x))),
        succeeded: json["succeeded"],
        errors: json["errors"],
        message: json["message"],
      );

  Map<String, dynamic> toJson() => {
        "pageNumber": pageNumber,
        "pageSize": pageSize,
        "firstPage": firstPage,
        "lastPage": lastPage,
        "totalPages": totalPages,
        "totalRecords": totalRecords,
        "nextPage": nextPage,
        "previousPage": previousPage,
        "data": List<dynamic>.from(data.map((x) => x.toJson())),
        "succeeded": succeeded,
        "errors": errors,
        "message": message,
      };
}

class DeviceType {
  DeviceType({
    this.id,
    this.name,
    this.createdTime,
    this.lastModified,
    this.isDisable,
    this.devices,
  });

  int id;
  String name;
  DateTime createdTime;
  DateTime lastModified;
  bool isDisable;
  dynamic devices;

  factory DeviceType.fromJson(Map<String, dynamic> json) => DeviceType(
        id: json["id"],
        name: json["name"],
        createdTime: DateTime.parse(json["createdTime"]),
        lastModified: DateTime.parse(json["lastModified"]),
        isDisable: json["isDisable"],
        devices: json["devices"],
      );

  Map<String, dynamic> toJson() => {
        "id": id,
        "name": name,
        "createdTime": createdTime.toIso8601String(),
        "lastModified": lastModified.toIso8601String(),
        "isDisable": isDisable,
        "devices": devices,
      };
}
