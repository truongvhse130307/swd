// To parse this JSON data, do
//
//     final counterModel = counterModelFromJson(jsonString);

import 'dart:convert';

CounterModel counterModelFromJson(String str) =>
    CounterModel.fromJson(json.decode(str));

String counterModelToJson(CounterModel data) => json.encode(data.toJson());

class CounterModel {
  CounterModel({
    this.pageNumber,
    this.pageSize,
    this.firstPage,
    this.lastPage,
    this.totalPages,
    this.totalRecords,
    this.nextPage,
    this.previousPage,
    this.data,
    this.succeeded,
    this.errors,
    this.message,
  });

  int pageNumber;
  int pageSize;
  String firstPage;
  String lastPage;
  int totalPages;
  int totalRecords;
  String nextPage;
  dynamic previousPage;
  List<Counter> data;
  bool succeeded;
  dynamic errors;
  dynamic message;

  factory CounterModel.fromJson(Map<String, dynamic> json) => CounterModel(
        pageNumber: json["pageNumber"],
        pageSize: json["pageSize"],
        firstPage: json["firstPage"],
        lastPage: json["lastPage"],
        totalPages: json["totalPages"],
        totalRecords: json["totalRecords"],
        nextPage: json["nextPage"],
        previousPage: json["previousPage"],
        data: List<Counter>.from(json["data"].map((x) => Counter.fromJson(x))),
        succeeded: json["succeeded"],
        errors: json["errors"],
        message: json["message"],
      );

  Map<String, dynamic> toJson() => {
        "pageNumber": pageNumber,
        "pageSize": pageSize,
        "firstPage": firstPage,
        "lastPage": lastPage,
        "totalPages": totalPages,
        "totalRecords": totalRecords,
        "nextPage": nextPage,
        "previousPage": previousPage,
        "data": List<dynamic>.from(data.map((x) => x.toJson())),
        "succeeded": succeeded,
        "errors": errors,
        "message": message,
      };
}

class Counter {
  Counter({
    this.id,
    this.description,
    this.createdTime,
    this.lastModified,
    this.isDisable,
    this.devices,
    this.category,
    this.store,
    this.masterCounterId,
    this.masterCounter,
    this.records,
    this.invoices,
  });

  int id;
  String description;
  DateTime createdTime;
  DateTime lastModified;
  bool isDisable;
  dynamic devices;
  dynamic category;
  dynamic store;
  dynamic masterCounterId;
  dynamic masterCounter;
  dynamic records;
  dynamic invoices;

  factory Counter.fromJson(Map<String, dynamic> json) => Counter(
        id: json["id"],
        description: json["description"],
        createdTime: DateTime.parse(json["createdTime"]),
        lastModified: DateTime.parse(json["lastModified"]),
        isDisable: json["isDisable"],
        devices: json["devices"],
        category: json["category"],
        store: json["store"],
        masterCounterId: json["masterCounterId"],
        masterCounter: json["masterCounter"],
        records: json["records"],
        invoices: json["invoices"],
      );

  Map<String, dynamic> toJson() => {
        "id": id,
        "description": description,
        "createdTime": createdTime.toIso8601String(),
        "lastModified": lastModified.toIso8601String(),
        "isDisable": isDisable,
        "devices": devices,
        "category": category,
        "store": store,
        "masterCounterId": masterCounterId,
        "masterCounter": masterCounter,
        "records": records,
        "invoices": invoices,
      };
}
