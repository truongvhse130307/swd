// To parse this JSON data, do
//
//     final invoiceModel = invoiceModelFromJson(jsonString);

import 'dart:convert';

import 'package:ews/models/counterInfo.dart';

InvoiceModel invoiceModelFromJson(String str) =>
    InvoiceModel.fromJson(json.decode(str));

String invoiceModelToJson(InvoiceModel data) => json.encode(data.toJson());

class InvoiceModel {
  InvoiceModel({
    this.pageNumber,
    this.pageSize,
    this.firstPage,
    this.lastPage,
    this.totalPages,
    this.totalRecords,
    this.nextPage,
    this.previousPage,
    this.data,
    this.succeeded,
    this.errors,
    this.message,
  });

  int pageNumber;
  int pageSize;
  String firstPage;
  String lastPage;
  int totalPages;
  int totalRecords;
  dynamic nextPage;
  dynamic previousPage;
  List<Invoice> data;
  bool succeeded;
  dynamic errors;
  dynamic message;

  factory InvoiceModel.fromJson(Map<String, dynamic> json) => InvoiceModel(
        pageNumber: json["pageNumber"],
        pageSize: json["pageSize"],
        firstPage: json["firstPage"],
        lastPage: json["lastPage"],
        totalPages: json["totalPages"],
        totalRecords: json["totalRecords"],
        nextPage: json["nextPage"],
        previousPage: json["previousPage"],
        data: List<Invoice>.from(json["data"].map((x) => Invoice.fromJson(x))),
        succeeded: json["succeeded"],
        errors: json["errors"],
        message: json["message"],
      );

  Map<String, dynamic> toJson() => {
        "pageNumber": pageNumber,
        "pageSize": pageSize,
        "firstPage": firstPage,
        "lastPage": lastPage,
        "totalPages": totalPages,
        "totalRecords": totalRecords,
        "nextPage": nextPage,
        "previousPage": previousPage,
        "data": List<dynamic>.from(data.map((x) => x.toJson())),
        "succeeded": succeeded,
        "errors": errors,
        "message": message,
      };
}

class Invoice {
  Invoice({
    this.id,
    this.image,
    this.countIndex,
    this.createdTime,
    this.counter,
    this.lastModified,
    this.cost,
    this.startDate,
    this.endDate,
  });

  int id;
  dynamic image;
  int countIndex;
  DateTime createdTime;
  Counter counter;
  DateTime lastModified;
  int cost;
  DateTime startDate;
  DateTime endDate;

  factory Invoice.fromJson(Map<String, dynamic> json) {
    if (json["counter"] != null) {
      return Invoice(
        id: json["id"],
        image: json["image"],
        countIndex: json["countIndex"],
        createdTime: DateTime.parse(json["createdTime"]),
        counter: Counter.fromJson(json["counter"]),
        lastModified: DateTime.parse(json["lastModified"]),
        cost: json["cost"],
        startDate: DateTime.parse(json["startDate"]),
        endDate: DateTime.parse(json["endDate"]),
      );
    }
    return Invoice(
      id: json["id"],
      image: json["image"],
      countIndex: json["countIndex"],
      createdTime: DateTime.parse(json["createdTime"]),
      lastModified: DateTime.parse(json["lastModified"]),
      cost: json["cost"],
      startDate: DateTime.parse(json["startDate"]),
      endDate: DateTime.parse(json["endDate"]),
    );
  }

  Map<String, dynamic> toJson() => {
        "id": id,
        "image": image,
        "countIndex": countIndex,
        "createdTime": createdTime.toIso8601String(),
        "counter": counter.toJson(),
        "lastModified": lastModified.toIso8601String(),
        "cost": cost,
        "startDate": startDate.toIso8601String(),
        "endDate": endDate.toIso8601String(),
      };
}
