// To parse this JSON data, do
//
//     final roleModel = roleModelFromJson(jsonString);

import 'dart:convert';

RoleModel roleModelFromJson(String str) => RoleModel.fromJson(json.decode(str));

String roleModelToJson(RoleModel data) => json.encode(data.toJson());

class RoleModel {
  RoleModel({
    this.pageNumber,
    this.pageSize,
    this.firstPage,
    this.lastPage,
    this.totalPages,
    this.totalRecords,
    this.nextPage,
    this.previousPage,
    this.data,
    this.succeeded,
    this.errors,
    this.message,
  });

  int pageNumber;
  int pageSize;
  String firstPage;
  String lastPage;
  int totalPages;
  int totalRecords;
  String nextPage;
  dynamic previousPage;
  List<Role> data;
  bool succeeded;
  dynamic errors;
  dynamic message;

  factory RoleModel.fromJson(Map<String, dynamic> json) => RoleModel(
        pageNumber: json["pageNumber"],
        pageSize: json["pageSize"],
        firstPage: json["firstPage"],
        lastPage: json["lastPage"],
        totalPages: json["totalPages"],
        totalRecords: json["totalRecords"],
        nextPage: json["nextPage"],
        previousPage: json["previousPage"],
        data: List<Role>.from(json["data"].map((x) => Role.fromJson(x))),
        succeeded: json["succeeded"],
        errors: json["errors"],
        message: json["message"],
      );

  Map<String, dynamic> toJson() => {
        "pageNumber": pageNumber,
        "pageSize": pageSize,
        "firstPage": firstPage,
        "lastPage": lastPage,
        "totalPages": totalPages,
        "totalRecords": totalRecords,
        "nextPage": nextPage,
        "previousPage": previousPage,
        "data": List<dynamic>.from(data.map((x) => x.toJson())),
        "succeeded": succeeded,
        "errors": errors,
        "message": message,
      };
}

class Role {
  Role({
    this.id,
    this.name,
    this.createdTime,
    this.lastModified,
    this.isDisable,
    this.users,
  });

  int id;
  String name;
  DateTime createdTime;
  DateTime lastModified;
  bool isDisable;
  dynamic users;

  factory Role.fromJson(Map<String, dynamic> json) => Role(
        id: json["id"],
        name: json["name"],
        createdTime: DateTime.parse(json["createdTime"]),
        lastModified: DateTime.parse(json["lastModified"]),
        isDisable: json["isDisable"],
        users: json["users"],
      );

  Map<String, dynamic> toJson() => {
        "id": id,
        "name": name,
        "createdTime": createdTime.toIso8601String(),
        "lastModified": lastModified.toIso8601String(),
        "isDisable": isDisable,
        "users": users,
      };
}
