// To parse this JSON data, do
//
//     final recordModel = recordModelFromJson(jsonString);

import 'dart:convert';

import 'package:ews/models/counterInfo.dart';

RecordModel recordModelFromJson(String str) =>
    RecordModel.fromJson(json.decode(str));

String recordModelToJson(RecordModel data) => json.encode(data.toJson());

class RecordModel {
  RecordModel({
    this.pageNumber,
    this.pageSize,
    this.firstPage,
    this.lastPage,
    this.totalPages,
    this.totalRecords,
    this.nextPage,
    this.previousPage,
    this.data,
    this.succeeded,
    this.errors,
    this.message,
  });

  int pageNumber;
  int pageSize;
  String firstPage;
  String lastPage;
  int totalPages;
  int totalRecords;
  String nextPage;
  dynamic previousPage;
  List<Record> data;
  bool succeeded;
  dynamic errors;
  dynamic message;

  factory RecordModel.fromJson(Map<String, dynamic> json) => RecordModel(
        pageNumber: json["pageNumber"],
        pageSize: json["pageSize"],
        firstPage: json["firstPage"],
        lastPage: json["lastPage"],
        totalPages: json["totalPages"],
        totalRecords: json["totalRecords"],
        nextPage: json["nextPage"],
        previousPage: json["previousPage"],
        data: List<Record>.from(json["data"].map((x) => Record.fromJson(x))),
        succeeded: json["succeeded"],
        errors: json["errors"],
        message: json["message"],
      );

  Map<String, dynamic> toJson() => {
        "pageNumber": pageNumber,
        "pageSize": pageSize,
        "firstPage": firstPage,
        "lastPage": lastPage,
        "totalPages": totalPages,
        "totalRecords": totalRecords,
        "nextPage": nextPage,
        "previousPage": previousPage,
        "data": List<dynamic>.from(data.map((x) => x.toJson())),
        "succeeded": succeeded,
        "errors": errors,
        "message": message,
      };
}

class Record {
  Record({
    this.id,
    this.countNumber,
    this.lastModifiedBy,
    this.createdTime,
    this.lastModified,
    this.counter,
    this.image,
  });

  int id;
  int countNumber;
  String lastModifiedBy;
  DateTime createdTime;
  DateTime lastModified;
  Counter counter;
  String image;

  factory Record.fromJson(Map<String, dynamic> json) => Record(
        id: json["id"],
        countNumber: json["countNumber"],
        lastModifiedBy:
            json["lastModifiedBy"] == null ? null : json["lastModifiedBy"],
        createdTime: DateTime.parse(json["createdTime"]),
        lastModified: DateTime.parse(json["lastModified"]),
        counter:
            json["counter"] == null ? null : Counter.fromJson(json["counter"]),
        image: json["image"] == null ? null : json["image"],
      );

  Map<String, dynamic> toJson() => {
        "id": id,
        "countNumber": countNumber,
        "lastModifiedBy": lastModifiedBy == null ? null : lastModifiedBy,
        "createdTime": createdTime.toIso8601String(),
        "lastModified": lastModified.toIso8601String(),
        "counter": counter == null ? null : counter.toJson(),
        "image": image == null ? null : image,
      };
}
