// To parse this JSON data, do
//
//     final deviceModel = deviceModelFromJson(jsonString);

import 'dart:convert';

DeviceModel deviceModelFromJson(String str) =>
    DeviceModel.fromJson(json.decode(str));

String deviceModelToJson(DeviceModel data) => json.encode(data.toJson());

class DeviceModel {
  DeviceModel({
    this.pageNumber,
    this.pageSize,
    this.firstPage,
    this.lastPage,
    this.totalPages,
    this.totalRecords,
    this.nextPage,
    this.previousPage,
    this.data,
    this.succeeded,
    this.errors,
    this.message,
  });

  int pageNumber;
  int pageSize;
  String firstPage;
  String lastPage;
  int totalPages;
  int totalRecords;
  String nextPage;
  dynamic previousPage;
  List<Device> data;
  bool succeeded;
  dynamic errors;
  dynamic message;

  factory DeviceModel.fromJson(Map<String, dynamic> json) => DeviceModel(
        pageNumber: json["pageNumber"],
        pageSize: json["pageSize"],
        firstPage: json["firstPage"],
        lastPage: json["lastPage"],
        totalPages: json["totalPages"],
        totalRecords: json["totalRecords"],
        nextPage: json["nextPage"],
        previousPage: json["previousPage"],
        data: List<Device>.from(json["data"].map((x) => Device.fromJson(x))),
        succeeded: json["succeeded"],
        errors: json["errors"],
        message: json["message"],
      );

  Map<String, dynamic> toJson() => {
        "pageNumber": pageNumber,
        "pageSize": pageSize,
        "firstPage": firstPage,
        "lastPage": lastPage,
        "totalPages": totalPages,
        "totalRecords": totalRecords,
        "nextPage": nextPage,
        "previousPage": previousPage,
        "data": List<dynamic>.from(data.map((x) => x.toJson())),
        "succeeded": succeeded,
        "errors": errors,
        "message": message,
      };
}

class Device {
  Device({
    this.id,
    this.name,
    this.brandName,
    this.createdTime,
    this.lastModified,
    this.isDisable,
    this.powerConsumption,
    this.energySavingRating,
  });

  int id;
  String name;
  String brandName;
  DateTime createdTime;
  DateTime lastModified;
  bool isDisable;
  int powerConsumption;
  int energySavingRating;

  factory Device.fromJson(Map<String, dynamic> json) => Device(
        id: json["id"],
        name: json["name"],
        brandName: json["brandName"],
        createdTime: DateTime.parse(json["createdTime"]),
        lastModified: DateTime.parse(json["lastModified"]),
        isDisable: json["isDisable"],
        powerConsumption: json["powerConsumption"],
        energySavingRating: json["energySavingRating"],
      );

  Map<String, dynamic> toJson() => {
        "id": id,
        "name": name,
        "brandName": brandName,
        "createdTime": createdTime.toIso8601String(),
        "lastModified": lastModified.toIso8601String(),
        "isDisable": isDisable,
        "powerConsumption": powerConsumption,
        "energySavingRating": energySavingRating,
      };
}
